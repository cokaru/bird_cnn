import os
import glob
import numpy as np
import cv2
from sklearn.utils import shuffle
###########################################
import tensorflow as tf
import train_config as config
import matplotlib.pyplot as plt
#
#https://jhui.github.io/2017/03/08/TensorFlow-Reading-data-threading-queues/
###########################################


def load_one_file_prediction(img_file, img_size):
    images = []
    image = cv2.imread(img_file)
    image = cv2.resize(image, (img_size, img_size), cv2.INTER_LINEAR)
    images.append(image)
    images = np.array(images)

    return images


def load_train(train_path, image_size, classes):
    images = []
    labels = []
    ids = []
    cls = []

    print('Reading training images')
    for fld in classes:   # assuming data directory has a separate folder for each class, and that each folder is named after the class
        index = classes.index(fld)
        print('Loading {} files (Index: {})'.format(fld, index))
        path = os.path.join(train_path, fld, '*g')
        files = glob.glob(path)
        for fl in files:
            image = cv2.imread(fl)
            image = cv2.resize(image, (image_size, image_size), cv2.INTER_CUBIC)
            images.append(image)

            clahe = cv2.createCLAHE(clipLimit=3., tileGridSize=(8,8))
            lab = cv2.cvtColor(image, cv2.COLOR_BGR2LAB)  # convert from BGR to LAB color space
            l, a, b = cv2.split(lab)  # split on 3 different channels
            l2 = clahe.apply(l)  # apply CLAHE to the L-channel
            lab = cv2.merge((l2,a,b))  # merge channels
            adjusted_image = cv2.cvtColor(lab, cv2.COLOR_LAB2BGR)  # convert from LAB to BGR

            #plt.imshow(image)
            #plt.show()

            #plt.imshow(adjusted_image)
            #plt.show()

            #plt.imshow(adjusted_image2)
            #plt.show()

            label = np.zeros(len(classes))
            label[index] = 1.0
            labels.append(label)

    return np.array(images), np.array(labels)


def load_test(test_path, image_size,classes):
  
    for class_name in classes:
        path = os.path.join(test_path,class_name, '*g')
        files = sorted(glob.glob(path))

        X_test = []
        print("Reading test images")
        for fl in files:
            flbase = os.path.basename(fl)
            print(fl)
            img = cv2.imread(fl)
            img = cv2.resize(img, (image_size, image_size), cv2.INTER_LINEAR)
            X_test.append(img)

  ### because we're not creating a DataSet object for the test images, normalization happens here
        X_test = np.array(X_test, dtype=np.uint8)
        X_test = X_test.astype('float32')
        X_test = X_test / 255

    return X_test



class DataSet(object):

  def __init__(self, images, labels):    
    self._num_examples = images.shape[0]
    images = images.astype(np.float32)
    images = np.multiply(images, 1.0 / 255.0)

    self._images = images
    self._labels = labels
    self._epochs_completed = 0
    self._index_in_epoch = 0


  @property
  def images(self):
    return self._images

  @property
  def labels(self):
    return self._labels

  @property
  def num_examples(self):
    return self._num_examples

  @property
  def epochs_completed(self):
    return self._epochs_completed

  def next_batch(self, batch_size):
    """Return the next `batch_size` examples from this data set."""
    start = self._index_in_epoch
    self._index_in_epoch += batch_size

    if self._index_in_epoch > self._num_examples:
      # Finished epoch
      self._epochs_completed += 1

      start = 0
      self._index_in_epoch = batch_size
      assert batch_size <= self._num_examples
    end = self._index_in_epoch

    return self._images[start:end], self._labels[start:end]


def read_train_sets(train_path, image_size, classes, validation_size=0):
  class DataSets(object):
    pass
  data_sets = DataSets()

  images, labels = load_train(train_path, image_size, classes)
  images, labels = shuffle(images, labels)  
 
  if isinstance(validation_size, float):
    validation_size = int(validation_size * images.shape[0])

  validation_images = images[:validation_size]
  validation_labels = labels[:validation_size]


  train_images = images[validation_size:]
  train_labels = labels[validation_size:]


  data_sets.train = DataSet(train_images, train_labels)
  data_sets.valid = DataSet(validation_images, validation_labels) 
  data_queue = tf.RandomShuffleQueue(capacity = config.batch_size,min_after_dequeue= 0,dtypes = [ tf.float32 ,tf.float32 ],shapes = [ 
      [ config.img_size, config.img_size, config.num_channels], [config.num_classes]        
      ] )
  #data_queue = tf.FIFOQueue(capacity = config.batch_size,dtypes = [ tf.float32 ,tf.float32 ],shapes = [ 
  #    [ config.img_size, config.img_size, config.num_channels], [config.num_classes]        
  #    ] )  
######################################################################################  
  return data_queue,  data_sets
  #return train_queue,  valid_queue, data_sets


def read_test_set(test_path, image_size,classes):
  images  = load_test(test_path, image_size,classes)
  return images

