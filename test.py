import numpy as np
import datetime as dt
now = dt.datetime.now()

a = np.arange(15).reshape(3, 5)
#s=repr(a).replace('array',' ')
#s='      '+''.join([ c for c in s if c not in ('(', ')','[',']',',')])
#a2 = np.fromstring(a.tostring(), dtype=np.float)
#print(s)
#s=repr(a[0][0]).replace('array',' ')
#print(s)
a_strings = ["%.2f" % x for x in a.reshape(a.size)]
print(a_strings)
print(a_strings[0])
elapsed_time = dt.datetime.now() - now
msg = ("info:{0} , message: {1} \n").format('elapsed time', elapsed_time)
print(msg)
