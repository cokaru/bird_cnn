import os, sys
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
import numpy as np
import tensorflow as tf
import train_config as config
import cnn_evaluate
import datetime as dt
 
window_width = 640
window_height= 480

################################################################
# References
# https://www.tutorialspoint.com/pyqt/pyqt_qfiledialog_widget.htm

################################################################
### App
class App(QMainWindow):
 
    def __init__(self):
        super().__init__()
        self.title = 'CNN - Guess  a speices of birds. - by sjpark'
        self.left = 0
        self.top = 0
        self.width = window_width
        self.height = window_height
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)
 
        self.table_widget = CNNWidget(self)
        self.setCentralWidget(self.table_widget)
 
        screen_center = lambda widget: QApplication.desktop().screen().rect().center()- widget.rect().center()
        self.move(screen_center(self))

        self.table_widget.initialize_tf()
        self.show()
 

################################################################
### CNNWidget
class CNNWidget(QWidget):        
 
    def __init__(self, parent):   
        super(QWidget, self).__init__(parent)
        self.layout = QVBoxLayout(self)
 
        self.evaluation_file ='' 
        self.tabs = QTabWidget()
        self.tab1 = QWidget()	
        self.tab2 = QWidget()
        self.tabs.resize(window_width , window_height) 
 
        self.tabs.addTab(self.tab1,"Evaluate")
        self.tabs.addTab(self.tab2,"Train")
 
        self.make_evaluate_ui(self.tab1)
        self.make_train_ui(self.tab2)

        self.layout.addWidget(self.tabs)
        self.setLayout(self.layout)

        ############################################
        ##tensorflow 
        self.session = None        
        self.saver = None
        self.tf_graph = None
        self.evaluate_model = cnn_evaluate.model()
        ############################################

    def make_evaluate_ui(self, tab_widget):
        self.model_file_text = QLineEdit()
        self.model_btn = QPushButton("Feed Me...")
        self.model_restore_btn = QPushButton("Restore Me.")
        self.model_evaluation_btn = QPushButton("Import Image File...")
        self.modle_evaluation_label = QLabel("Evaluation File : ")

        self.evaluate_btn = QPushButton("Evaluate.")
        self.model_evaluation_img = QLabel()
        self.model_evaluation_result = QLabel()
        self.model_output_msg = QPlainTextEdit()
        
        self.model_btn.clicked.connect(self.feed_me)
        self.model_evaluation_btn.clicked.connect(self.show_eval_img)
        self.evaluate_btn.clicked.connect(self.evaluate)

        layout = QVBoxLayout(self)

        layout_1 = QHBoxLayout();
        layout_1.addWidget(QLabel("Model Data : "));        
        layout_1.addWidget(self.model_file_text)        
        layout_1.addWidget(self.model_btn);

        layout_2 = QHBoxLayout()
        layout_2.addWidget(self.model_restore_btn)

        layout_3 = QHBoxLayout()
        layout_3.addWidget(self.modle_evaluation_label)       
        layout_3.addWidget(self.model_evaluation_btn)
        layout_3.addWidget(self.evaluate_btn)
        
        layout_4 = QHBoxLayout()
        layout_4.addWidget(self.model_evaluation_img)
        layout_4.addWidget(self.model_evaluation_result)

        layout_5 = QHBoxLayout();
        layout_5.addWidget(self.model_output_msg)

        layout.addLayout(layout_1)
        layout.addLayout(layout_2)
        layout.addLayout(layout_3)
        layout.addLayout(layout_4)
        layout.addLayout(layout_5)

        tab_widget.setLayout(layout)

    def make_train_ui(self, tab_widget):
        pass

    def feed_me(self):
        now = dt.datetime.now()
        meta_filename = QFileDialog.getOpenFileName(self, 'Open file', os.getcwd() ,"Tensor flow meta's file (*.meta)")                
        self.model_file_text.insert(meta_filename[0])
        self.add_model_output_msg('The selected meta file', meta_filename[0])
        self.saver, self.tf_graph = self.evaluate_model.restore_network(self.session, meta_filename[0])        
        elapsed_time = dt.datetime.now() - now
        self.add_model_output_msg('Restore Data - Elapsed Time' ,elapsed_time)

    def show_eval_img(self):
        img_filename = QFileDialog.getOpenFileName(self, 'Open file', os.getcwd() ,"Evaluation image's file (*.jpg *.jpeg)")
        self.add_model_output_msg('The evaluation image file', img_filename[0])
        self.model_evaluation_img.setPixmap(QPixmap(img_filename[0]))
        self.model_evaluation_img.setFixedSize(1280, 720) 
        self.evaluation_file = img_filename[0]
        self.modle_evaluation_label.setText('Evaluation File : ' + os.path.basename(img_filename[0]))

    def add_model_output_msg(self, header, content):
        msg = ("info:{0} , message: {1} \n").format(header, content)
        self.model_output_msg.insertPlainText(msg)
        self.model_output_msg.update()

    def initialize_tf(self):
        self.session = tf.InteractiveSession()
        self.session.run(tf.global_variables_initializer())

    def evaluate(self):
        now = dt.datetime.now()
        pred, answer = self.evaluate_model.evalute_prediction(self.session,  self.evaluation_file, config.img_size)
        prediction =  self.get_python_string(pred)
        who_are_you = self.get_python_string(answer)

        self.add_model_output_msg('The prediction of your image : ', prediction)
        self.add_model_output_msg('The class of your image : ', who_are_you)

        result_msg = ('THE RESULT OF PREDICTION :  \n  -Black throated Sparrow :  {0}\n  -Cardinal: {1}\n  -Ovenbird: {2}\n  -Red eyed vireo: {3}\n  -Red headed woodpecker: {4}\n\n' .
                      format(prediction[0],prediction[1],prediction[2],prediction[3],prediction[4]))
        result_msg += ('A SPECIES OF BIRD IS  ' + self.get_class(int(float(who_are_you[0]))))
        self.model_evaluation_result.setText(result_msg)
        elapsed_time = dt.datetime.now() - now
        self.add_model_output_msg('Evaluation - Elapsed Time' ,elapsed_time)


    def get_python_string(self, np_array):
        values =  ["%.4f" % x for x in np_array.reshape(np_array.size)]
        return values

    def get_class(self, class_index):
        bird_class = {
            0 : 'Black throated Sparrow',
            1 : 'Cardinal',
            2 : 'Ovenbird',
            3 : 'Red eyed vireo',
            4 : ' Red headed woodpecker',
            }
        return bird_class[class_index]



if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec_())