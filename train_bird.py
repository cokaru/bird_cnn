import matplotlib.pyplot as plt
import tensorflow as tf
import pandas as pd
import numpy as np
import math
import dataset
import train_config as config

#######################################################################
##https://www.tensorflow.org/programmers_guide/threading_and_queues
##https://www.tensorflow.org/api_guides/python/io_ops
##https://jhui.github.io/2017/03/08/TensorFlow-Reading-data-threading-queues/
##http://bcho.tistory.com/1163
##https://www.tensorflow.org/api_docs/python/tf/FIFOQueue
##https://stackoverflow.com/questions/36334371/tensorflow-batching-input-queues-then-changing-the-queue-source
##https://tensorflowkorea.gitbooks.io/tensorflow-kr/content/g3doc/how_tos/threading_and_queues/
##https://tensorflowkorea.gitbooks.io/tensorflow-kr/content/g3doc/how_tos/reading_data/
#######################################################################

data_queue,  data = dataset.read_train_sets(config.train_path, config.img_size, config.classes, validation_size=config.validation_size)

print("Size of:")
print("- Training-set:\t\t{}".format(len(data.train.labels)))

def new_weights(shape):    
    return tf.Variable(tf.truncated_normal(shape, stddev=0.05))

def new_biases(length):
    return tf.Variable(tf.constant(0.05, shape=[length]))

def new_conv_layer(input, num_input_channels, filter_size,  num_filters, use_pooling=True): 

    shape = [filter_size, filter_size, num_input_channels, num_filters]
    weights = new_weights(shape=shape)     
    biases = new_biases(length=num_filters)

    layer = tf.nn.conv2d(input=input,filter=weights,strides=[1, 1, 1, 1],padding='SAME')
    layer += biases
    
    if use_pooling:
        layer = tf.nn.max_pool(value=layer,ksize=[1, 2, 2, 1],strides=[1, 2, 2, 1],padding='SAME')

    layer = tf.nn.relu(layer)
    return layer, weights

def flatten_layer(layer):
    layer_shape = layer.get_shape()
    print('flatten_layer- layer_shape')
    print(layer_shape)
    num_features = layer_shape[1:4].num_elements()

    layer_flat = tf.reshape(layer, [-1, num_features])
    return layer_flat, num_features

def new_fc_layer(input, num_inputs, num_outputs, use_relu=True): 
    
    weights = new_weights(shape=[num_inputs, num_outputs])
    biases = new_biases(length=num_outputs)
    layer = tf.matmul(input, weights) + biases

    if use_relu:
        layer = tf.nn.relu(layer)
    return layer

session = tf.Session()

x = tf.placeholder(tf.float32, shape=[None, config.img_size_flat], name='x')
x_image = tf.reshape(x, [-1, config.img_size, config.img_size, config.num_channels])

y_true = tf.placeholder(tf.float32, shape=[None, config.num_classes], name='y_true')
y_true_cls = tf.argmax(y_true, dimension=1)

layer_conv1, weights_conv1 = \
new_conv_layer(input=x_image,
               num_input_channels=config.num_channels,
               filter_size=config.filter_size1,
               num_filters=config.num_filters1,
               use_pooling=True)
print("now layer2 input")
print(layer_conv1.get_shape())     

layer_conv2, weights_conv2 = \
new_conv_layer(input=layer_conv1,
               num_input_channels=config.num_filters1,
               filter_size=config.filter_size2,
               num_filters=config.num_filters2,
               use_pooling=True)
print("now layer3 input")
print(layer_conv2.get_shape())     
               
layer_conv3, weights_conv3 = \
new_conv_layer(input=layer_conv2,
               num_input_channels=config.num_filters2,
               filter_size=config.filter_size3,
               num_filters=config.num_filters3,
               use_pooling=True)
print("now layer flatten input")
print(layer_conv3.get_shape())     
          
layer_flat, num_features = flatten_layer(layer_conv3)

layer_fc1 = new_fc_layer(input=layer_flat,
                     num_inputs=num_features,
                     num_outputs=config.fc_size,
                     use_relu=True)

layer_fc2 = new_fc_layer(input=layer_fc1,
                     num_inputs=config.fc_size,
                     num_outputs=config.num_classes,
                     use_relu=False)

y_pred = tf.nn.softmax(layer_fc2,name='y_pred')
y_pred_cls = tf.argmax(y_pred, dimension=1, name='y_pred_cls')

cross_entropy = tf.nn.softmax_cross_entropy_with_logits(logits=layer_fc2 ,labels=y_true, name='cross_entropy')
cost = tf.reduce_mean(cross_entropy, name='cost')

correct_prediction = tf.equal(y_pred_cls, y_true_cls, name='correct_prediction')
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32) , name='accuracy')

optimizer = tf.train.AdamOptimizer(learning_rate=1e-4).minimize(cost)
tf.add_to_collection('optimizer' , optimizer)

train_batch_size = config.batch_size
######################################################################################
session.run(tf.global_variables_initializer()) 
######################################################################################

def print_progress(epoch, feed_dict_train, feed_dict_validate, val_loss):
    acc = session.run(accuracy, feed_dict=feed_dict_train)
    val_acc = session.run(accuracy, feed_dict=feed_dict_validate)
    msg = "Epoch {0} --- Training Accuracy: {1:>6.1%}, Validation Accuracy: {2:>6.1%}, Validation Loss: {3:.3f}"
    print(msg.format(epoch + 1, acc, val_acc, val_loss))

total_iterations = 0
#######################################################################

train_queue_op = data_queue.enqueue_many( [data.train.images, data.train.labels] )
img_data, label_data = data_queue.dequeue()
qr = tf.train.QueueRunner(data_queue, [ train_queue_op] * config.num_threads)
#######################################################################

def optimize(num_iterations):
    global total_iterations

    coord = tf.train.Coordinator()
    enqueue_threads = qr.create_threads(session, coord=coord, start=True)

    for i in range(total_iterations, total_iterations + num_iterations):
        if coord.should_stop() :
            print('iteration is stopped :  coord.shoud_stop') 
            break;

        print('Iteration : ' + str(i) +'/' + str(num_iterations))
        x_batch =[] 
        y_true_batch = []
        x_valid_batch =[]
        y_valid_batch =[]

        for step in range(config.batch_size):
            train_image, train_label = session.run([img_data, label_data])
            x_batch.append(train_image)
            y_true_batch.append(train_label)
           
            adjusted_image = train_image.reshape(config.img_size,config.img_size,config.num_channels)

            flip_image = tf.image.flip_left_right(adjusted_image)            
            x_batch.append(session.run(flip_image))
            y_true_batch.append(train_label)

            birghtness_image = tf.image.random_brightness(adjusted_image, max_delta = 0.3)
            x_batch.append(session.run(birghtness_image))
            y_true_batch.append(train_label)

            constrast_image = tf.image.random_contrast(adjusted_image, lower= 0.2, upper=1.8)
            x_batch.append(session.run(constrast_image))
            y_true_batch.append(train_label)
                                        
        x_batch = np.array(x_batch)
        y_true_batch = np.array(y_true_batch)
        
        for step in range(config.batch_size):
            valid_image, valid_label = session.run([img_data, label_data])            

            x_valid_batch.append(valid_image)
            y_valid_batch.append(valid_label)

        x_valid_batch = np.array(x_valid_batch)
        y_valid_batch = np.array(y_valid_batch)
               
        x_batch = x_batch.reshape(train_batch_size * 4, config.img_size_flat )
        x_valid_batch = x_valid_batch.reshape(train_batch_size, config.img_size_flat)
        
        feed_dict_train = {x: x_batch,y_true: y_true_batch}        
        feed_dict_validate = {x: x_valid_batch,y_true: y_valid_batch}

        session.run(optimizer, feed_dict=feed_dict_train)
        if (i % 30) == 0:
           saver = tf.train.Saver()
           saver.save(session, '.\\bird_test_model') 

        val_loss = session.run(cost, feed_dict=feed_dict_validate)
        epoch = int(i / int(data.train.num_examples/config.batch_size))            
        print_progress(epoch, feed_dict_train, feed_dict_validate, val_loss)


    total_iterations += num_iterations

    coord.request_stop()
    coord.join(enqueue_threads)


optimize(num_iterations=1500)  

